
<?php 

require 'fb-init.php';

if (!isset($_SESSION['access_token']) && !isset($_SESSION['email_login']) || empty(setcookie('user', '')) ) {
 	header("Location:login.php");
}


$date_now = date("Y-m-d");
$expired = false;
$aktif	= false;
if (isset($_SESSION['license_type'])) {
	if ($_SESSION['license_type'] == "Pro") {
		if ($date_now <= $_SESSION['end_license']) {
			$aktif = true;			
		}else{						
			$expired = true;
		}
	}
}


// show data
$email = $_SESSION['email_login'];
$query = "SELECT * FROM mdl_license WHERE email = '$email' ";
$result =  mysql_fetch_assoc(mysql_query("SELECT * FROM mdl_license WHERE email = '$email' ",$connection));

// update data
if(isset($_POST['update'])){
    $email = $_SESSION['email_login'];
    $fullname = $_POST['fullname'];
    $password = $_POST['password'];

    if( !empty($fullname) && !empty($password) ){
        // update fullname and password
        $password = md5($password);
        $update = mysql_query("UPDATE mdl_license SET fullname='$fullname', password='$password' WHERE email='$email' ", $connection);
        if($update){
          $_SESSION['fullname']= $fullname;
          $_SESSION['password']= $password;
          header('Location: profile.php');
        }
    }elseif( !empty($fullname) && empty($password) ){
        // update fullname
        $update = mysql_query("UPDATE mdl_license SET fullname='$fullname' WHERE email='$email' ", $connection);
        if($update){
          $_SESSION['fullname']= $fullname;
          header('Location: profile.php');
        }
    }else{
        // update password
        $password = md5($password);
        $update = mysql_query("UPDATE mdl_license SET password='$password' WHERE email='$email' ", $connection);
        if ($update) {
          $_SESSION['password']= $password;
          header('Location: profile.php');
        }
    }

}
require 'header_templates.php';

?>


          <!-- //////////////////////////////////////////////////////////////////////////// -->
          <!-- START CONTENT -->
          <section id="content">
            <!--breadcrumbs start-->
            <!-- <div id="breadcrumbs-wrapper"> -->
              <!-- Search for small screen -->
              <!-- <div class="header-search-wrapper grey lighten-2 hide-on-large-only"> -->
                <!-- <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"> -->
              <!-- </div> -->
            <!-- </div> -->
            <!--breadcrumbs end-->
            <!--start container-->
            <div class="section no-pad-bot" id="index-banner">

          <div class="container">

            <!-- alert update profile -->
            <?php 
            if($_SESSION['fullname'] != $_SESSION['fullname_alert'] && $_SESSION['password'] != $_SESSION['password_alert'] ){
            $_SESSION['fullname_alert'] = $result['fullname'];
            $_SESSION['password_alert'] = $result['password'];
            ?>

              <div id="card-alert" class="card green lighten-5">
                <div class="card-content green-text">
                  <p>Fullname and password updated successfully!</p>
                </div>
                <button type="button" class="close green-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>

            <?php 
            }elseif($_SESSION['fullname'] != $_SESSION['fullname_alert']){ 
            $_SESSION['fullname_alert'] = $result['fullname'];
            ?>

              <div id="card-alert" class="card green lighten-5">
                <div class="card-content green-text">
                  <p>Fullname updated successfully!</p>
                </div>
                <button type="button" class="close green-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>

            <?php 
            }elseif($_SESSION['password'] != $_SESSION['password_alert']){ 
            $_SESSION['password_alert'] = $result['password'];
            ?>

              <div id="card-alert" class="card green lighten-5">
                <div class="card-content green-text">
                  <p>Password updated successfully!</p>
                </div>
                <button type="button" class="close green-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>

            <?php 
            }else{
            ?>
              <div id="card-alert" class="card cyan lighten-5">
                <div class="card-content cyan-text">
                  <p>Change data?</p>
                </div>
                <button type="button" class="close cyan-text" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
             <?php 
            }
            ?>
            <!-- alert update profile -->

            <!-- Inline form with placeholder -->
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="card-panel">
                  <div class="row">
                    <form class="col s12" method="post">
                      <h4 class="header2">Profile user</h4>
                      <div class="row">
                        <div class="input-field col s4">
                          <i class="material-icons prefix">account_circle</i>
                          <input value="<?= $result['fullname']; ?>" id="icon_prefix2" name="fullname" type="text" class="validate">
                          <label for="icon_prefix">Fullname</label>
                        </div>
                        <div class="input-field col s4">
                          <i class="material-icons prefix">lock_outline</i>
                          <input placeholder="password" id="icon_password" name="password" type="password" class="validate">
                          <label for="icon_password">Password</label>
                        </div>
                        <div class="input-field col s4">
                          <div class="input-field col s12">
                            <button class="btn cyan waves-effect waves-light" type="submit" name="update">
                            Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            

          </div>

      		</section>
      		<!-- END CONTENT -->
      		<!-- //////////////////////////////////////////////////////////////////////////// -->      		
      	</div>
      	<!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
<?php 
require 'footer_templates.php';
 ?>