<!DOCTYPE html>
<html lang="en">

  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="msapplication-tap-highlight" content="no">
  	<meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
  	<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
  	<title>Home - Andropedia</title>
  	<link rel="apple-touch-icon" sizes="57x57" href="assets/fav/apple-icon-57x57.png">
  	<link rel="apple-touch-icon" sizes="60x60" href="assets/fav/apple-icon-60x60.png">
  	<link rel="apple-touch-icon" sizes="72x72" href="assets/fav/apple-icon-72x72.png">
  	<link rel="apple-touch-icon" sizes="76x76" href="assets/fav/apple-icon-76x76.png">
  	<link rel="apple-touch-icon" sizes="114x114" href="assets/fav/apple-icon-114x114.png">
  	<link rel="apple-touch-icon" sizes="120x120" href="assets/fav/apple-icon-120x120.png">
  	<link rel="apple-touch-icon" sizes="144x144" href="assets/fav/apple-icon-144x144.png">
  	<link rel="apple-touch-icon" sizes="152x152" href="assets/fav/apple-icon-152x152.png">
  	<link rel="apple-touch-icon" sizes="180x180" href="assets/fav/apple-icon-180x180.png">
  	<link rel="icon" type="image/png" sizes="192x192"  href="assets/fav/android-icon-192x192.png">
  	<link rel="icon" type="image/png" sizes="32x32" href="assets/fav/favicon-32x32.png">
  	<link rel="icon" type="image/png" sizes="96x96" href="assets/fav/favicon-96x96.png">
  	<link rel="icon" type="image/png" sizes="16x16" href="assets/fav/favicon-16x16.png">
  	<link rel="manifest" href="assets/fav/manifest.json">
  	<meta name="msapplication-TileColor" content="#ffffff">
  	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  	<meta name="theme-color" content="#ffffff">
  	<!-- For Windows Phone -->
  	<!-- CORE CSS-->
  	<link href="html/starter-kit/css/themes/semi-dark-menu/materialize.css" type="text/css" rel="stylesheet">
  	<link href="html/starter-kit/css/themes/semi-dark-menu/style.css" type="text/css" rel="stylesheet">
    <!-- Custome CSS-->
    <link href="html/starter-kit/css/custom/custom.css" type="text/css" rel="stylesheet">
  	<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  	<link href="html/starter-kit/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
  	<link href="html/materialize-admin/vendors/magnific-popup/magnific-popup.css" type="text/css" rel="stylesheet">
  	<link href="html/starter-kit/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="assets/style.css">
  </head>
  <body class="layout-semi-dark">
  	<!-- Start Page Loading -->
  	<div id="loader-wrapper">
  		<div id="loader"></div>
  		<div class="loader-section section-left"></div>
  		<div class="loader-section section-right"></div>
  	</div>
  	<!-- End Page Loading -->
  	<!-- //////////////////////////////////////////////////////////////////////////// -->
  	<!-- START HEADER -->
  	<header id="header" class="page-topbar">
  		<!-- start header nav-->
  		<div class="navbar-fixed">
  			<nav class="navbar-color">
  				<div class="nav-wrapper">
            <ul class="right hide-on-med-and-down">
            	<li>
            		<a href="#">
            			<?php echo $_SESSION['fullname']; ?> <?php if ($aktif) { echo'<span class="new badge red" data-badge-caption="Pro"></span>';}else{echo'<span class="new badge blue" data-badge-caption="Standart"></span>';} ?>
            		</a>
            	</li>
            	<li>
            		<a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen">
            			<i class="material-icons">settings_overscan</i>
            		</a>
            	</li>
            	<li>
            		<a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
            			<span class="avatar-status avatar-online">
            				<img src="html/starter-kit/images/avatar/avatar-7.png" alt="avatar">
            				<i></i>
            			</span>
            		</a>
            	</li>
            </ul>

          <!-- profile-dropdown -->

            <ul id="profile-dropdown" class="dropdown-content">
            	<li>
            		<a href="profile.php" class="grey-text text-darken-1">
            		<i class="material-icons">face</i> Profile</a>
            	</li>
            	<li>
            		<a href="#" class="grey-text text-darken-1">
            		<i class="material-icons">live_help</i> Help</a>
            	</li>
            	<li class="divider"></li>
            	<li>
            		<a href="logout.php" class="grey-text text-darken-1">
            		<i class="material-icons">keyboard_tab</i> Logout</a>
            	</li>
            </ul>
      		</div>
      	</nav>
      </div>
    </header>
      <!-- END HEADER -->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START MAIN -->
      <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">
          <!-- START LEFT SIDEBAR NAV-->
          <aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible">
            <div class="brand-sidebar">
              <h1 class="logo-wrapper">
                <a href="index.php" class="brand-logo darken-1">
                  <div class="logo-res">
                    <img src="assets/logo.png" alt="materialize logo">
                  </div>
                  <!-- <span class="logo-text hide-on-med-and-down">Andropedia </span> -->
                </a>
                <a href="#" class="navbar-toggler">
                  <i class="material-icons">radio_button_checked</i>
                </a>
              </h1>
            </div>
            <ul id="slide-out" class="side-nav fixed leftside-navigation">
              <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                  <li class="bold">
                    <a class="waves-effect waves-cyan" href="index.php">
                      <i class="material-icons">dashboard</i>
                      <span class="nav-text">Home</span>
                    </a>
                  </li>
<!--                   <li class="bold">
                    <a class=" waves-effect waves-cyan" href="search.php">
                      <i class="material-icons">search</i>
                      <span class="nav-text">Search</span>
                    </a>
                  </li> -->

                  <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan">
                      <i class="material-icons">search</i>
                      <span class="nav-text">Search</span>
                    </a>
                    <div class="collapsible-body">
                      <ul>
                        <li>
                          <a href="search.php">
                            <i class="material-icons">location_searching</i>
                            <span>Play Store</span>
                          </a>
                        </li>
                        <li>
                          <a href="searchapp.php">
                            <i class="material-icons">location_searching</i>
                            <span>AppBrain</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li class="bold">
                    <a class=" waves-effect waves-cyan" href="list.php">
                      <i class="material-icons">bookmark</i>
                      <span class="nav-text">Bookmark</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="li-hover">
                <p class="ultra-small margin more-text">MORE</p>
              </li>
              <li>
                <a href="html/starter-kit/documentation" target="_blank">
                  <i class="material-icons">import_contacts</i>
                  <span class="nav-text">Documentation</span>
                </a>
              </li>
              <li>
                <a href="https://pixinvent.ticksy.com" target="_blank">
                  <i class="material-icons">help_outline</i>
                  <span class="nav-text">Support</span>
                </a>
              </li>
              <ul class="hide-on-large-only">
                <li class="li-hover">
                  <p class="ultra-small margin more-text">Account</p>
                </li>
                <li>
                  <a href="profile.php" target="_blank">
                    <i class="material-icons">face</i>
                    <span class="nav-text">Profile</span>
                  </a>
                </li>
                <li>
                  <a href="https://pixinvent.ticksy.com" target="_blank">
                    <i class="material-icons">live_help</i>
                    <span class="nav-text">Help</span>
                  </a>
                </li>
                <li>
                  <a href="logout.php" target="_blank">
                    <i class="material-icons">keyboard_tab</i>
                    <span class="nav-text">Logout</span>
                  </a>
                </li>
              </ul>
            </ul>
            <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">
              <i class="material-icons">menu</i>
            </a>
          </aside>
          <!-- END LEFT SIDEBAR NAV-->