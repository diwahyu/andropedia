<?php

require 'fb-init.php';
require 'conf.php';


if (isset($_SESSION['access_token']) || isset($_SESSION['email_login'])) {
  header("Location:index.php");
}
$error=''; // Variable To Store Error Message
if (isset($_POST['submit'])) {
  if (empty($_POST['username']) || empty($_POST['password'])) {
    $error = "Email or Password is invalid";
  }
  else
  {
// Define $username and $password
    $username=$_POST['username'];
    $password=$_POST['password'];

// To protect MySQL injection for Security purpose
    $username = stripslashes($username);
    $password = stripslashes($password);
    $username = mysql_real_escape_string($username);
    $password = md5(mysql_real_escape_string($password));

// SQL query to fetch information of registerd users and finds user match.
    $query = mysql_query("SELECT * FROM mdl_license WHERE email='$username' AND password='$password' ", $connection);
    $rows = mysql_num_rows($query);
    $data = mysql_fetch_array($query);
    if($data['status'] == 0){
      if ($rows == 1) {
        $_SESSION['email_login']=$username; // Initializing Session
        $_SESSION['fullname']=$data['fullname'];
        $_SESSION['password']=$data['password'];
        $_SESSION['fullname_alert']=$data['fullname']; //alert
        $_SESSION['password_alert']=$data['password']; //alert
        $_SESSION['license_type']=$data['license_type'];
        $_SESSION['start_license']=$data['start_license'];
        $_SESSION['end_license']=$data['end_license'];
        setcookie('user', $data['fullname'], time() + (60 * 60 * 6), '/');
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $query = mysql_query("UPDATE mdl_license SET waktu_login = CURRENT_TIMESTAMP, browser='$browser', status=1 WHERE email='$username' ", $connection);
        header("location: ./index.php"); // Redirecting To Other Page
      } else {
          $error = "Username or Password is invalid";
      }
    }else{
       $error = "Akun sedang atau masih dipakai". $data['browser'];
    }
  mysql_close($connection); // Closing Connection
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon-16x16.png">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>AndroPedia</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link rel="stylesheet" href="assets/style.css">

</head>
<body class="body">
  <div class="panel">
    <ul class="panel__menu" id="menu">
      <hr/>
      <?php 
      // echo $_SERVER['HTTP_USER_AGENT']; 
      ?>
      <li id="signIn"> <a href="#">Login</a></li>
      <li id="signUp"><a href="#">Facebook Login</a></li>
    </ul>
    <div class="panel__wrap">
      <div class="panel__box active" id="signInBox">
        <form method="POST">
          <label>Email
            <input type="email" name="username" required="required" />
          </label>
          
          <label>Password
            <input type="password" name="password" required="required"  />
          </label>
          <label><small class="alert" style="color: red"><?php echo $error;?></small></label>
          <input type="submit" name="submit" value="Login" />
        </form>
      </div>
      <div class="panel__box" id="signUpBox">
        <center>
          <button id="download-button" onclick="location.href='<?php echo $loginurl;?>';" class="loginBtn loginBtn--facebook">Facebook Login</button>
        </center>
      </div>
    </div>
  </div>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="assets/app.js"></script>
  </html>