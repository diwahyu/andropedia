
<?php 

require 'fb-init.php';

if (!isset($_SESSION['access_token']) && !isset($_SESSION['email_login']) || empty(setcookie('user', '')) ){
	header("Location:login.php");
}

// echo $_SESSION['access_token'];
// echo $_SESSION['email_login'];
// echo $_SESSION['fullname'];
// echo $_SESSION['license_type'];
// echo $_SESSION['start_license'];
// echo $_SESSION['end_license'];
$date_now = date("Y-m-d");
$expired = false;
$aktif  = false;
if (isset($_SESSION['license_type'])) {
  if ($_SESSION['license_type'] == "Pro") {
    if ($date_now <= $_SESSION['end_license']) {
      $aktif = true;      
    }else{            
      $expired = true;
    }
  }
}

require 'header_templates.php';

?>

      		<!-- //////////////////////////////////////////////////////////////////////////// -->
      		<!-- START CONTENT -->
      		<section id="content">
      			<!--breadcrumbs start-->
      			<!-- <div id="breadcrumbs-wrapper"> -->
      				<!-- Search for small screen -->
      				<!-- <div class="header-search-wrapper grey lighten-2 hide-on-large-only"> -->
      					<!-- <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"> -->
      				<!-- </div> -->
      			<!-- </div> -->
      			<!--breadcrumbs end-->
      			<!--start container-->
      			<div class="section no-pad-bot" id="index-banner">

		<div class="container">
			<?php if ($expired) {
				echo '<div class="card-panel red white-text center">Paket lisensi anda telah expired, silahkan perbaru lagi. Paket Anda kembali ke Standart</div>';
			}?>			
			<br><br>
			<h1 class="header center green-text darken-2">
				<div class="img-res">
					<img src="assets/andropedia-logo2.png">
				</div>
			</h1>
			<div class="row center">
				<?php 
				// echo date('s:i:h d-m-Y');
				 ?>
				<h5 class="header col s12 light">Pilih Parameter yang sesuai dengan keinginan Anda</h5>
			</div>
			<div class="row center">
				<div class="col s12 z-depth-2" >
					<br>
					<form class="col s12" method="post" >
						<div class="row">
							<div class="input-field col s12 m4">
								<select class="icons" name="country" id="country">
									<option value="" disabled selected>Pilih Kategori</option>
									<optgroup label="Apps">
										<option value="art-and-design" >Art & Design</option>
										<option value="auto-and-vehicles" >Auto & Vehicles</option>
										<option value="beauty" >Beauty</option>
										<option value="books-and-reference" >Books & Reference</option>
										<option value="business" >Business</option>
										<option value="comics" >Comics</option>
										<option value="communication" >Communication</option>
										<option value="dating" >Dating</option>
										<option value="education" >Education</option>
										<option value="entertainment" >Entertainment</option>
										<option value="events" >Events</option>
										<option value="finance" >Finance</option>
										<option value="food-and-drink" >Food & Drink</option>
										<option value="health-and-fitness" >Health & Fitness</option>
										<option value="house-and-home" >House & Home</option>
										<option value="libraries-and-demo" >Libraries & Demo</option>
										<option value="lifestyle" >Lifestyle</option>
										<option value="maps-and-navigation" >Maps & Navigation</option>
										<option value="medical" >Medical</option>
										<option value="music-and-audio" >Music & Audio</option>
										<option value="news-and-magazines" >News & Magazines</option>
										<option value="parenting" >Parenting</option>
										<option value="personalization" >Personalization</option>
										<option value="photography" >Photography</option>
										<option value="productivity" >Productivity</option>
										<option value="shopping" >Shopping</option>
										<option value="social" >Social</option>
										<option value="sports" >Sports</option>
										<option value="tools" >Tools</option>
										<option value="travel-and-local" >Travel & Local</option>
										<option value="video-players-and-editors" >Video Players & Editors</option>
										<option value="weather" >Weather</option>
									</optgroup>
									<optgroup label="Games">
										<option value="action" >Action</option>
										<option value="adventure" >Adventure</option>
										<option value="arcade" >Arcade</option>
										<option value="board-games" >Board</option>
										<option value="card-games" >Card</option>
										<option value="casino" >Casino</option>
										<option value="casual" >Casual</option>
										<option value="educational" >Educational</option>
										<option value="music-games" >Music</option>
										<option value="puzzle" >Puzzle</option>
										<option value="racing" >Racing</option>
										<option value="role-playing" >Role Playing</option>
										<option value="simulation" >Simulation</option>
										<option value="sports-games" >Sports</option>
										<option value="strategy" >Strategy</option>
										<option value="trivia" >Trivia</option>
										<option value="word-games" >Word</option>
									</optgroup>
								</select>
								<label>Pilih Kategori</label>
							</div>
							<div class="input-field col s12 m4">
								<select class="icons" name="category" id="category">
									<option value="" disabled selected>Pilih Kategori</option>
									<option value="hot" >Hot Today</option>
									<option value="hot-week" >Recent Donwloads</option>
									<option value="popular" >All-time Donwloads</option>
									<option value="highest-rated" >Top Rated</option>
									<option value="highest-rated" >Top Rated</option>
									<option value="latest" >Time</option>
								</select>
								<label>Pilih Sortir</label>
							</div>
							<div class="input-field col s12 m4">
								<select class="icons" name="total" id="total">
									<option value="off" >Off</option>
									<option value="free" >Free</option>
									<option value="paid" >Paid</option>
									<option value="new" >New</option>
									<option value="updated" >updated</option>
									<option value="price-drop" >Price Reduced</option>
								</select>
								<label>Pilih Filter</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12">
								<div class="center">
									<button name="type" type="button" onclick="scrape('topselling_free',0,0);" value="free" style="margin: 5px" class="btn-large waves-effect waves-light gradient-45deg-amber-amber box-shadow-none gas">TOP FREE</button>
									<?php
										if ($aktif) {
											echo '<button name="type" type="button" onclick="scrape(\'topselling_new_free\',0,0);" value="new" style="margin: 5px" class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-green-teal gas">TOP NEW</button>
											<button name="type" type="button" onclick="scrape(\'topgrossing\',0,0);" value="groosing" style="margin: 5px" class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-indigo-blue gas">TOP GROOSING</button>
											<button name="type" type="button" onclick="scrape(\'topselling_paid\',0,0);" value="free" style="margin: 5px" class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-purple-deep-orange gas">TOP PAID</button>
											<button name="type" type="button" onclick="scrape(\'topselling_new_paid\',0,0);" value="new" style="margin: 5px" class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-light-blue-cyan gas">TOP NEW PAID</button>';
										}else{
											echo '<button name="type" type="button" value="new" style="margin: 5px" disabled class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-green-teal">TOP NEW</button>
											<button name="type" type="button" value="groosing" style="margin: 5px" disabled class="btn-large waves-effect waves-light box-shadow-none ">TOP GROOSING</button>
											<button name="type" type="button" value="free" style="margin: 5px" disabled class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-purple-deep-orange">TOP PAID</button>
											<button name="type" type="button" value="new" style="margin: 5px" disabled class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-light-blue-cyan">TOP NEW PAID</button>';
										}						
									
									?>
								</div>
							</div>
							<div class="input-field col m12 s12" style="padding: 10px">
								<?php
									if ($aktif) {
										echo '<button name="type" type="button" onclick="scrape(\'movers_shakers\',0,0);" value="groosing" class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-red-pink gas"><i class="material-icons left">attach_money</i> movers shakers <i class="material-icons right">attach_money</i></button>';
									}else{
										echo '<button name="type" type="button" disabled value="groosing" class="btn-large waves-effect waves-light box-shadow-none gradient-45deg-red-pink"><i class="material-icons left">attach_money</i> movers shakers <i class="material-icons right">attach_money</i></button>';
									}						
								
								?>								
							</div>
						</div>						
					</form>				
				</div>
			</div>				

			<br><br>

		</div>
	</div>

	<div class="container">
		<div class="section">

			<!--   Icon Section   -->
			<div class="row">
				<div class="col s12 m12">
					<div class="icon-block">
						<h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
						<h5 class="center">Hasil Scrape</h5>
						<div class="row">
							<div id="hasilScrape"></div>
						</div>						
						<br>
						<div id="loadingbos" class="progress" style="display: none;">
							<div class="indeterminate"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br><br>
	</div>

      			<!--end container-->
      		</section>
      		<!-- END CONTENT -->
      		<!-- //////////////////////////////////////////////////////////////////////////// -->      		
      	</div>
      	<!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
 <?php 
require 'footer_templates.php';
  ?>