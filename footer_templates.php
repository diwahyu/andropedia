     <!-- START FOOTER -->
      <footer class="page-footer">
        <div class="footer-copyright">
          <div class="container">
            <span>Copyright ©
              <script type="text/javascript">
                document.write(new Date().getFullYear());
              </script> <a class="grey-text text-darken-2" href="https://andropedia.id/" target="_blank">ANDROPEDIA</a> All rights reserved.
            </span>
            <span class="right hide-on-small-only"> Design and Developed by <a class="grey-text text-darken-2" href="https://andropedia.id/">Andropedia</a> | Made with ❤ in Solo</span>
          </div>
        </div>
      </footer>
      <!-- END FOOTER -->
      <!-- ================================================
      Scripts
      ================================================ -->
      <!-- jQuery Library -->
      <script type="text/javascript" src="html/starter-kit/vendors/jquery-3.2.1.min.js"></script>
      <!--materialize js-->
      <script type="text/javascript" src="html/starter-kit/js/materialize.min.js"></script>
      <!--scrollbar-->
      <script type="text/javascript" src="html/starter-kit/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
      <!-- masonry -->
      <script src="html/materialize-admin/vendors/masonry.pkgd.min.js"></script>
      <!--plugins.js - Some Specific JS codes for Plugin Settings-->
      <script type="text/javascript" src="html/starter-kit/js/plugins.js"></script>
      <!-- imagesloaded -->
      <script src="html/materialize-admin/vendors/imagesloaded.pkgd.min.js"></script>
      <!-- magnific-popup -->
      <script type="text/javascript" src="html/materialize-admin/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
      <!--plugins.js - Some Specific JS codes for Plugin Settings-->
      <script type="text/javascript" src="html/materialize-admin/js/plugins.js"></script>
      <!--media-gallary-page.js - Page specific JS-->
      <script type="text/javascript" src="html/materialize-admin/js/scripts/media-gallary-page.js"></script>
      <!-- chartjs -->
      <script type="text/javascript" src="html/materialize-admin/vendors/chartjs/chart.min.js"></script>
      <!--custom-script.js - Add your own theme custom JS-->
      <script type="text/javascript" src="html/starter-kit/js/custom-script.js"></script>
      <script src="assets/main.js"></script>
  </body>
  </html>