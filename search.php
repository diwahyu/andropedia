
<?php 

require 'fb-init.php';

if (!isset($_SESSION['access_token']) && !isset($_SESSION['email_login']) || empty(setcookie('user', '')) ) {
	header("Location:login.php");
}

$date_now = date("Y-m-d");
$expired = false;
$aktif	= false;
if (isset($_SESSION['license_type'])) {
	if ($_SESSION['license_type'] == "Pro") {
		if ($date_now <= $_SESSION['end_license']) {
			$aktif = true;			
		}else{						
			$expired = true;
		}
	}
}

require 'header_templates.php';

?>


      		<!-- //////////////////////////////////////////////////////////////////////////// -->
      		<!-- START CONTENT -->
      		<section id="content">
            <!--breadcrumbs start-->
            <!-- <div id="breadcrumbs-wrapper"> -->
              <!-- Search for small screen -->
              <!-- <div class="header-search-wrapper grey lighten-2 hide-on-large-only"> -->
                <!-- <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"> -->
              <!-- </div> -->
            <!-- </div> -->
            <!--breadcrumbs end-->
            <!--start container-->
	<div class="section no-pad-bot" id="index-banner">

		<div class="container">
			<?php if ($expired) {
				echo '<div class="card-panel red white-text center">Paket lisensi anda telah expired, silahkan perbaru lagi. Paket Anda kembali ke Standart</div>';
			}?>			
			<br><br>
			<h1 class="header center green-text darken-2">
				<div class="img-res">
					<img src="assets/andropedia-logo2.png">
				</div>
			</h1>
			<div class="row center">
				<h5 class="header col s12 light">Pilih Parameter yang sesuai dengan keinginan Anda</h5>
			</div>
			<div class="row center">
				<div class="col s12 z-depth-2" >
					<br>
					<form class="col s12" method="post" >
						<div class="row">
							<div class="input-field col s12">
								<input placeholder="Masukkan kata kunci" name="keyword" id="keyword" type="text" class="validate">
								<label for="first_name">Keyword</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12 m4">
								<select class="icons" name="country" id="country">
									<option value="" disabled selected>Pilih Negara</option>
									<option value="AL" data-hl="en" >Albania</option>
									<option value="DZ" data-hl="en" >Algeria</option>
									<option value="AS" data-hl="en" >American Samoa</option>
									<option value="AO" data-hl="en" >Angola</option>
									<option value="AG" data-hl="en" >Antigua and Barbuda</option>
									<option value="AR" data-hl="es" >Argentina</option>
									<option value="AM" data-hl="hy" >Armenia</option>
									<option value="AW" data-hl="en" >Aruba</option>
									<option value="AU" data-hl="en" >Australia</option>
									<option value="AT" data-hl="de" >Austria</option>
									<option value="BS" data-hl="en" >Bahamas</option>
									<option value="BH" data-hl="ar" >Bahrain</option>
									<option value="BD" data-hl="en" >Bangladesh</option>
									<option value="BY" data-hl="be" >Belarus</option>
									<option value="BE" data-hl="nl" >Belgium</option>
									<option value="BZ" data-hl="en" >Belize</option>
									<option value="BJ" data-hl="fr" >Benin</option>
									<option value="BO" data-hl="es" >Bolivia</option>
									<option value="BA" data-hl="bs" >Bosnia and Herzegovina</option>
									<option value="BW" data-hl="en" >Botswana</option>
									<option value="BR" data-hl="pt" >Brazil</option>
									<option value="bg" data-hl="bg" >Bulgaria</option>
									<option value="bf" data-hl="fr" >Burkina Faso</option>
									<option value="kh" data-hl="en" >Cambodia</option>
									<option value="cm" data-hl="fr" >Cameroon</option>
									<option value="ca" data-hl="en" >Canada</option>
									<option value="cv" data-hl="fr" >Cape Verde</option>
									<option value="cl" data-hl="es" >Chile</option>
									<option value="cn" data-hl="zh" >China</option>
									<option value="cr" data-hl="es" >Costa Rica</option>
									<option value="ci" data-hl="fr" >CÃ´te d'Ivoire</option>
									<option value="hr" data-hl="hr" >Croatia</option>
									<option value="cy" data-hl="el" >Cyprus</option>
									<option value="cz" data-hl="cs" >Czech Republic</option>
									<option value="dk" data-hl="da" >Denmark</option>
									<option value="do" data-hl="es" >Dominican Republic</option>
									<option value="ec" data-hl="es" >Ecuador</option>
									<option value="eg" data-hl="ar" >Egypt</option>
									<option value="sv" data-hl="es" >El Salvador</option>
									<option value="cq" data-hl="es" >Equatorial Guinea</option>
									<option value="ee" data-hl="et" >Estonia</option>
									<option value="et" data-hl="am" >Ethiopia</option>
									<option value="fj" data-hl="en" >Fiji</option>
									<option value="fr" data-hl="fr" >France</option>
									<option value="ga" data-hl="fr" >Gabon</option>
									<option value="de" data-hl="de" >Germany</option>
									<option value="gh" data-hl="en" >Ghana</option>
									<option value="gi" data-hl="en" >Gibraltar</option>
									<option value="gr" data-hl="el" >Greece</option>
									<option value="gu" data-hl="en" >Guam</option>
									<option value="gt" data-hl="es" >Guatemala</option>
									<option value="gw" data-hl="pt" >Guinea-Bissau</option>
									<option value="ht" data-hl="ht" >Haiti</option>
									<option value="hn" data-hl="es" >Honduras</option>
									<option value="hk" data-hl="zh" >Hong Kong</option>
									<option value="hu" data-hl="hu" >Hungary</option>
									<option value="is" data-hl="en" >Iceland</option>
									<option value="in" data-hl="hi" >India</option>
									<option value="id" data-hl="id" >Indonesia</option>
									<option value="ie" data-hl="en" >Ireland</option>
									<option value="il" data-hl="he" >Israel</option>
									<option value="it" data-hl="it" >Italy</option>
									<option value="jm" data-hl="en" >Jamaica</option>
									<option value="jp" data-hl="ja" >Japan</option>
									<option value="jo" data-hl="ar" >Jordan</option>
									<option value="kz" data-hl="kk" >Kazakhstan</option>
									<option value="ke" data-hl="en" >Kenya</option>
									<option value="kr" data-hl="ko" >Korea Republic of</option>
									<option value="kw" data-hl="ar" >Kuwait</option>
									<option value="kg" data-hl="ky" >Kyrgyzstan</option>
									<option value="la" data-hl="fr" >Lao People's Democratic Republic</option>
									<option value="lv" data-hl="lv" >Latvia</option>
									<option value="lb" data-hl="ar" >Lebanon</option>
									<option value="li" data-hl="de" >Liechtenstein</option>
									<option value="lt" data-hl="lt" >Lithuania</option>
									<option value="lu" data-hl="lb" >Luxembourg</option>
									<option value="mk" data-hl="mk" >Macedonia, the Former Yugoslav Republic of</option>
									<option value="my" data-hl="ms" >Malaysia</option>
									<option value="ml" data-hl="fr" >Mali</option>
									<option value="mt" data-hl="en" >Malta</option>
									<option value="mh" data-hl="mh" >Marshall Islands</option>
									<option value="mu" data-hl="ht" >Mauritius</option>
									<option value="mx" data-hl="es" >Mexico</option>
									<option value="md" data-hl="ro" >Moldova, Republic of</option>
									<option value="in" data-hl="en" >Morocco</option>
									<option value="mz" data-hl="pt" >Mozambique</option>
									<option value="mm" data-hl="my" >Myanmar</option>
									<option value="na" data-hl="en" >Namibia</option>
									<option value="np" data-hl="ne" >Nepal</option>
									<option value="nl" data-hl="nl" >Netherlands</option>
									<option value="nz" data-hl="en" >New Zealand</option>
									<option value="ni" data-hl="es" >Nicaragua</option>
									<option value="ne" data-hl="fr" >Niger</option>
									<option value="ng" data-hl="en" >Nigeria</option>
									<option value="nu" data-hl="en" >Niue</option>
									<option value="mp" data-hl="en" >Northern Mariana Islands</option>
									<option value="no" data-hl="no" >Norway</option>
									<option value="om" data-hl="ar" >Oman</option>
									<option value="pk" data-hl="en" >Pakistan</option>
									<option value="pw" data-hl="en" >Palau</option>
									<option value="pa" data-hl="es" >Panama</option>
									<option value="pg" data-hl="en" >Papua New Guinea</option>
									<option value="py" data-hl="es" >Paraguay</option>
									<option value="pe" data-hl="es" >Peru</option>
									<option value="ph" data-hl="en" >Philippines</option>
									<option value="pl" data-hl="pl" >Poland</option>
									<option value="pt" data-hl="pt" >Portugal</option>
									<option value="pr" data-hl="es" >Puerto Rico</option>
									<option value="qa" data-hl="ar" >Qatar</option>
									<option value="re" data-hl="fr" >RÃ©union</option>
									<option value="ro" data-hl="ro" >Romania</option>
									<option value="ru" data-hl="ru" >Russian Federation</option>
									<option value="rw" data-hl="rw" >Rwanda</option>
									<option value="sa" data-hl="ar" >Saudi Arabia</option>
									<option value="sn" data-hl="fr" >Senegal</option>
									<option value="rs" data-hl="sr" >Serbia</option>
									<option value="sg" data-hl="en" >Singapore</option>
									<option value="sk" data-hl="sk" >Slovakia</option>
									<option value="si" data-hl="sl" >Slovenia</option>
									<option value="za" data-hl="en" >South Africa</option>
									<option value="es" data-hl="es" >Spain</option>
									<option value="lk" data-hl="si" >Sri Lanka</option>
									<option value="se" data-hl="se" >Sweden</option>
									<option value="ch" data-hl="de" >Switzerland</option>
									<option value="tw" data-hl="zh" >Taiwan, Province of China</option>
									<option value="tj" data-hl="tg" >Tajikistan</option>
									<option value="th" data-hl="th" >Thailand</option>
									<option value="tg" data-hl="fr" >Togo</option>
									<option value="tt" data-hl="en" >Trinidad and Tobago</option>
									<option value="tn" data-hl="fr" >Tunisia</option>
									<option value="tr" data-hl="tr" >Turkey</option>
									<option value="tm" data-hl="tk" >Turkmenistan</option>
									<option value="ug" data-hl="en" >Uganda</option>
									<option value="ua" data-hl="uk" >Ukraine</option>
									<option value="ae" data-hl="ar" >United Arab Emirates</option>
									<option value="gb" data-hl="en" >United Kingdom</option>
									<option value="us" data-hl="en" >United States</option>
									<option value="uy" data-hl="es" >Uruguay</option>
									<option value="uz" data-hl="uz" >Uzbekistan</option>
									<option value="ve" data-hl="es" >Venezuela, Bolivarian Republic of</option>
									<option value="vn" data-hl="vi" >Viet Nam</option>
									<option value="vg" data-hl="en" >Virgin Islands, British</option>
									<option value="vi" data-hl="en" >Virgin Islands, U.S.</option>
									<option value="ye" data-hl="ar" >Yemen</option>
									<option value="zm" data-hl="en" >Zambia</option>
									<option value="zw" data-hl="en" >Zimbabwe</option>

								</select>
								<label>Pilih Negara</label>
							</div>
							<div class="input-field col s12 m4">
								<select class="icons" name="pricing" id="pricing">
									<option value="0" >Semua Harga</option>
									<option value="1" >Gratis</option>
									<option value="2" >Berbayar</option>
								</select>
								<label>Pilih Pricing</label>
							</div>
							<div class="input-field col s12 m4">
								<select class="icons" name="rating" id="rating">
									<option value="0" >All Rating</option>
									<option value="1" >4 Bintang+ </option>
								</select>
								<label>Pilih Rating</label>
							</div>
						</div>

						<div class="row">
							<div class="input-field col m12 s12" style="padding: 10px">
								<?php
								if ($aktif) {
									echo '<button name="type" type="button" onclick="search();" value="groosing" class="btn-large waves-effect waves-light red gas"><i class="material-icons left">attach_money</i> Search <i class="material-icons right">attach_money</i></button>';
								}else{
									echo '<button name="type" type="button" disabled value="groosing" class="btn-large waves-effect waves-light red"><i class="material-icons left">attach_money</i> Search <i class="material-icons right">attach_money</i></button>';
								}						
								
								?>								
							</div>
						</div>						
					</form>				
				</div>
			</div>				

			<br><br>

		</div>
	</div>

	<div class="container">
		<div class="section">

			<!--   Icon Section   -->
			<div class="row">
				<div class="col s12 m12">
					<div class="icon-block">
						<h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
						<h5 class="center">Hasil Scrape</h5>
						<div class="row">
							<div id="hasilSearch"></div>
						</div>						
						<br>
						<div id="loadingbos" class="progress" style="display: none;">
							<div class="indeterminate"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br><br>
	</div>

      				<!--end container-->
      			</section>
      			<!-- END CONTENT -->
      			<!-- //////////////////////////////////////////////////////////////////////////// -->      		
      		</div>
      		<!-- END WRAPPER -->
      	</div>
      	<!-- END MAIN -->
      	<!-- //////////////////////////////////////////////////////////////////////////// -->
 <?php 
require 'footer_templates.php';
  ?>