
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<title>Riset ❤ Keyword</title>

	<!-- CSS  -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<style type="text/css">
	.icon-block {
		padding: 0 15px;
	}
	.icon-block .material-icons {
		font-size: inherit;
	}
</style>
</head>
<body>
	<nav class="light-blue lighten-1" role="navigation">
		<div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">KW ❤ Riset</a>
			<ul class="right hide-on-med-and-down">
				<li><a href="#">Riset ❤ Keyword</a></li>
			</ul>

			<ul id="nav-mobile" class="side-nav">
				<li><a href="#">Riset ❤ Keyword</a></li>
			</ul>
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
		</div>
	</nav>
	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<br><br>
			<h1 class="header center orange-text">Riset ❤ Keyword</h1>
			<div class="row center">
				<h5 class="header col s12 light">Pilih Parameter yang sesuai dengan keinginan Anda</h5>
			</div>
			<div class="row center">
				<div class="col s12 z-depth-2" >
					<br>
					<form class="col s12" method="post" >
						<div class="row">
							<div class="input-field col s12 m6">
								<select class="icons" name="country" id="country">
									<option value="" disabled selected>Pilih Negara</option>
									<option value="AL" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Albania.png" class="left circle">Albania</option>
									<option value="DZ" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Algeria.png" class="left circle">Algeria</option>
									<option value="AS" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Samoa.png" class="left circle">American Samoa</option>
									<option value="AO" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Angola.png" class="left circle">Angola</option>
									<option value="AG" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Antigua.png" class="left circle">Antigua and Barbuda</option>
									<option value="AR" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Argentina.png" class="left circle">Argentina</option>
									<option value="AM" data-hl="hy" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Armenia.png" class="left circle">Armenia</option>
									<option value="AW" data-hl="en" data-icon="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Flag_of_Aruba.svg/2000px-Flag_of_Aruba.svg.png" class="left circle">Aruba</option>
									<option value="AU" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Australia.png" class="left circle">Australia</option>
									<option value="AT" data-hl="de" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Austria.png" class="left circle">Austria</option>
									<option value="BS" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Bahamas.png" class="left circle">Bahamas</option>
									<option value="BH" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Bahrain.png" class="left circle">Bahrain</option>
									<option value="BD" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Bangladesh.png" class="left circle">Bangladesh</option>
									<option value="BY" data-hl="be" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Belarus.png" class="left circle">Belarus</option>
									<option value="BE" data-hl="nl" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Belgium.png" class="left circle">Belgium</option>
									<option value="BZ" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Belize.png" class="left circle">Belize</option>
									<option value="BJ" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Benin.png" class="left circle">Benin</option>
									<option value="BO" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Bolivia.png" class="left circle">Bolivia</option>
									<option value="BA" data-hl="bs" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Bosnia-Herzegovina.png" class="left circle">Bosnia and Herzegovina</option>
									<option value="BW" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Botswana.png" class="left circle">Botswana</option>
									<option value="BR" data-hl="pt" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Brazil.png" class="left circle">Brazil</option>
									<option value="bg" data-hl="bg" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Bulgaria.png" class="left circle">Bulgaria</option>
									<option value="bf" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Burkina-Faso.png" class="left circle">Burkina Faso</option>
									<option value="kh" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Cambodia.png" class="left circle">Cambodia</option>
									<option value="cm" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Cameroon.png" class="left circle">Cameroon</option>
									<option value="ca" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Canada.png" class="left circle">Canada</option>
									<option value="cv" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Cabo-Verde.png" class="left circle">Cape Verde</option>
									<option value="cl" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Chile.png" class="left circle">Chile</option>
									<option value="cn" data-hl="zh" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-China.png" class="left circle">China</option>
									<option value="cr" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Costa-Rica.png" class="left circle">Costa Rica</option>
									<option value="ci" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Cote-d-Ivoire.png" class="left circle">CÃ´te d'Ivoire</option>
									<option value="hr" data-hl="hr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Croatia.png" class="left circle">Croatia</option>
									<option value="cy" data-hl="el" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Cyprus.png" class="left circle">Cyprus</option>
									<option value="cz" data-hl="cs" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Czech-Republic.png" class="left circle">Czech Republic</option>
									<option value="dk" data-hl="da" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Denmark.png" class="left circle">Denmark</option>
									<option value="do" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Dominican-Republic.png" class="left circle">Dominican Republic</option>
									<option value="ec" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Ecuador.png" class="left circle">Ecuador</option>
									<option value="eg" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Egypt.png" class="left circle">Egypt</option>
									<option value="sv" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-El-Salvador.png" class="left circle">El Salvador</option>
									<option value="cq" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Equatorial-Guinea.png" class="left circle">Equatorial Guinea</option>
									<option value="ee" data-hl="et" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Estonia.png" class="left circle">Estonia</option>
									<option value="et" data-hl="am" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Ethiopia.png" class="left circle">Ethiopia</option>
									<option value="fj" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Fiji.png" class="left circle">Fiji</option>
									<option value="fr" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-France.png" class="left circle">France</option>
									<option value="ga" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Gabon.png" class="left circle">Gabon</option>
									<option value="de" data-hl="de" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Germany.png" class="left circle">Germany</option>
									<option value="gh" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Ghana.png" class="left circle">Ghana</option>
									<option value="gi" data-hl="en" data-icon="https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Flag_of_Gibraltar.svg/255px-Flag_of_Gibraltar.svg.png" class="left circle">Gibraltar</option>
									<option value="gr" data-hl="el" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Greece.png" class="left circle">Greece</option>
									<option value="gu" data-hl="en" data-icon="https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Flag_of_Guam.svg/2000px-Flag_of_Guam.svg.png" class="left circle">Guam</option>
									<option value="gt" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Guatemala.png" class="left circle">Guatemala</option>
									<option value="gw" data-hl="pt" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Guinea-Bissau.png" class="left circle">Guinea-Bissau</option>
									<option value="ht" data-hl="ht" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Haiti.png" class="left circle">Haiti</option>
									<option value="hn" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Honduras.png" class="left circle">Honduras</option>
									<option value="hk" data-hl="zh" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Honduras.png" class="left circle">Hong Kong</option>
									<option value="hu" data-hl="hu" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Hungary.png" class="left circle">Hungary</option>
									<option value="is" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Iceland.png" class="left circle">Iceland</option>
									<option value="in" data-hl="hi" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-India.png" class="left circle">India</option>
									<option value="id" data-hl="id" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Indonesia.png" class="left circle">Indonesia</option>
									<option value="ie" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Ireland.png" class="left circle">Ireland</option>
									<option value="il" data-hl="he" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Israel.png" class="left circle">Israel</option>
									<option value="it" data-hl="it" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Italy.png" class="left circle">Italy</option>
									<option value="jm" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Jamaica.png" class="left circle">Jamaica</option>
									<option value="jp" data-hl="ja" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Japan.png" class="left circle">Japan</option>
									<option value="jo" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Jordan.png" class="left circle">Jordan</option>
									<option value="kz" data-hl="kk" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Kazakhstan.png" class="left circle">Kazakhstan</option>
									<option value="ke" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Kenya.png" class="left circle">Kenya</option>
									<option value="kr" data-hl="ko" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Korea-South.png" class="left circle">Korea Republic of</option>
									<option value="kw" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Kuwait.png" class="left circle">Kuwait</option>
									<option value="kg" data-hl="ky" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Kyrgyzstan.png" class="left circle">Kyrgyzstan</option>
									<option value="la" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Laos.png" class="left circle">Lao People's Democratic Republic</option>
									<option value="lv" data-hl="lv" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Latvia.png" class="left circle">Latvia</option>
									<option value="lb" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Lebanon.png" class="left circle">Lebanon</option>
									<option value="li" data-hl="de" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Liechtenstein.png" class="left circle">Liechtenstein</option>
									<option value="lt" data-hl="lt" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Lithuania.png" class="left circle">Lithuania</option>
									<option value="lu" data-hl="lb" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Luxembourg.png" class="left circle">Luxembourg</option>
									<option value="mk" data-hl="mk" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Macedonia.png" class="left circle">Macedonia, the Former Yugoslav Republic of</option>
									<option value="my" data-hl="ms" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Malaysia.png" class="left circle">Malaysia</option>
									<option value="ml" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Mali.png" class="left circle">Mali</option>
									<option value="mt" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Malta.png" class="left circle">Malta</option>
									<option value="mh" data-hl="mh" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Marshall-Islands.png" class="left circle">Marshall Islands</option>
									<option value="mu" data-hl="ht" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Mauritius.png" class="left circle">Mauritius</option>
									<option value="mx" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Mexico.png" class="left circle">Mexico</option>
									<option value="md" data-hl="ro" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Moldova.png" class="left circle">Moldova, Republic of</option>
									<option value="in" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Morocco.png" class="left circle">Morocco</option>
									<option value="mz" data-hl="pt" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Mozambique.png" class="left circle">Mozambique</option>
									<option value="mm" data-hl="my" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Myanmar.png" class="left circle">Myanmar</option>
									<option value="na" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Namibia.png" class="left circle">Namibia</option>
									<option value="np" data-hl="ne" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Nepal.png" class="left circle">Nepal</option>
									<option value="nl" data-hl="nl" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Netherlands.png" class="left circle">Netherlands</option>
									<option value="nz" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-New-Zealand.png" class="left circle">New Zealand</option>
									<option value="ni" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Nicaragua.png" class="left circle">Nicaragua</option>
									<option value="ne" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Niger.png" class="left circle">Niger</option>
									<option value="ng" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Nigeria.png" class="left circle">Nigeria</option>
									<option value="nu" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Niue.png" class="left circle">Niue</option>
									<option value="mp" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Norway.png" class="left circle">Northern Mariana Islands</option>
									<option value="no" data-hl="no" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Norway.png" class="left circle">Norway</option>
									<option value="om" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Oman.png" class="left circle">Oman</option>
									<option value="pk" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Pakistan.png" class="left circle">Pakistan</option>
									<option value="pw" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Palau.png" class="left circle">Palau</option>
									<option value="pa" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Panama.png" class="left circle">Panama</option>
									<option value="pg" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Papua-New-Guinea.png" class="left circle">Papua New Guinea</option>
									<option value="py" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Paraguay.png" class="left circle">Paraguay</option>
									<option value="pe" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Peru.png" class="left circle">Peru</option>
									<option value="ph" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Philippines.png" class="left circle">Philippines</option>
									<option value="pl" data-hl="pl" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Poland.png" class="left circle">Poland</option>
									<option value="pt" data-hl="pt" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Portugal.png" class="left circle">Portugal</option>
									<option value="pr" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Puerto.png" class="left circle">Puerto Rico</option>
									<option value="qa" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Qatar.png" class="left circle">Qatar</option>
									<option value="re" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-RÃ©union.png" class="left circle">RÃ©union</option>
									<option value="ro" data-hl="ro" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Romania.png" class="left circle">Romania</option>
									<option value="ru" data-hl="ru" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Russia.png" class="left circle">Russian Federation</option>
									<option value="rw" data-hl="rw" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Rwanda.png" class="left circle">Rwanda</option>
									<option value="sa" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Saudi-Arabia.png" class="left circle">Saudi Arabia</option>
									<option value="sn" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Senegal.png" class="left circle">Senegal</option>
									<option value="rs" data-hl="sr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Serbia.png" class="left circle">Serbia</option>
									<option value="sg" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Singapore.png" class="left circle">Singapore</option>
									<option value="sk" data-hl="sk" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Slovakia.png" class="left circle">Slovakia</option>
									<option value="si" data-hl="sl" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Slovenia.png" class="left circle">Slovenia</option>
									<option value="za" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-South-Africa.png" class="left circle">South Africa</option>
									<option value="es" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Spain.png" class="left circle">Spain</option>
									<option value="lk" data-hl="si" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-South-Africa.png" class="left circle">Sri Lanka</option>
									<option value="se" data-hl="se" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Sweden.png" class="left circle">Sweden</option>
									<option value="ch" data-hl="de" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Switzerland.png" class="left circle">Switzerland</option>
									<option value="tw" data-hl="zh" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Taiwan.png" class="left circle">Taiwan, Province of China</option>
									<option value="tj" data-hl="tg" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Tajikistan.png" class="left circle">Tajikistan</option>
									<option value="th" data-hl="th" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Thailand.png" class="left circle">Thailand</option>
									<option value="tg" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Togo.png" class="left circle">Togo</option>
									<option value="tt" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Trinidad-and-Tobago.png" class="left circle">Trinidad and Tobago</option>
									<option value="tn" data-hl="fr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Tunisia.png" class="left circle">Tunisia</option>
									<option value="tr" data-hl="tr" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Turkey.png" class="left circle">Turkey</option>
									<option value="tm" data-hl="tk" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Turkmenistan.png" class="left circle">Turkmenistan</option>
									<option value="ug" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Uganda.png" class="left circle">Uganda</option>
									<option value="ua" data-hl="uk" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Ukraine.png" class="left circle">Ukraine</option>
									<option value="ae" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Arab-Emirates.png" class="left circle">United Arab Emirates</option>
									<option value="gb" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png" class="left circle">United Kingdom</option>
									<option value="us" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-United-States-of-America.png" class="left circle">United States</option>
									<option value="uy" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Uruguay.png" class="left circle">Uruguay</option>
									<option value="uz" data-hl="uz" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Uzbekistan.png" class="left circle">Uzbekistan</option>
									<option value="ve" data-hl="es" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Venezuela.png" class="left circle">Venezuela, Bolivarian Republic of</option>
									<option value="vn" data-hl="vi" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Vietnam.png" class="left circle">Viet Nam</option>
									<option value="vg" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Virgin.png" class="left circle">Virgin Islands, British</option>
									<option value="vi" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Virgin.png" class="left circle">Virgin Islands, U.S.</option>
									<option value="ye" data-hl="ar" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Yemen.png" class="left circle">Yemen</option>
									<option value="zm" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Zambia.png" class="left circle">Zambia</option>
									<option value="zw" data-hl="en" data-icon="https://www.countries-ofthe-world.com/flags-normal/flag-of-Zimbabwe.png" class="left circle">Zimbabwe</option>

								</select>
								<label>Pilih Negara</label>
							</div>
							<div class="input-field col s12 m6">
								<select class="icons" name="category" id="category">
									<option value="" disabled selected>Pilih Kategori</option>
									<option value="BOOKS_AND_REFERENCE" >BOOKS_AND_REFERENCE</option>
									<option value="BUSINESS" >BUSINESS</option>
									<option value="COMICS" >COMICS</option>
									<option value="COMMUNICATION" >COMMUNICATION</option>
									<option value="EDUCATION" >EDUCATION</option>
									<option value="ENTERTAINMENT" >ENTERTAINMENT</option>
									<option value="FINANCE" >FINANCE</option>
									<option value="HEALTH_AND_FITNESS" >HEALTH_AND_FITNESS</option>
									<option value="LIBRARIES_AND_DEMO" >LIBRARIES_AND_DEMO</option>
									<option value="LIFESTYLE" >LIFESTYLE</option>
									<option value="APP_WALLPAPER" >APP_WALLPAPER</option>
									<option value="MEDIA_AND_VIDEO" >MEDIA_AND_VIDEO</option>
									<option value="MEDICAL" >MEDICAL</option>
									<option value="MUSIC_AND_AUDIO" >MUSIC_AND_AUDIO</option>
									<option value="NEWS_AND_MAGAZINES" >NEWS_AND_MAGAZINES</option>
									<option value="PERSONALIZATION" >PERSONALIZATION</option>
									<option value="PHOTOGRAPHY" >PHOTOGRAPHY</option>
									<option value="PRODUCTIVITY" >PRODUCTIVITY</option>
									<option value="SHOPPING" >SHOPPING</option>
									<option value="SOCIAL" >SOCIAL</option>
									<option value="SPORTS" >SPORTS</option>
									<option value="TOOLS" >TOOLS</option>
									<option value="TRANSPORTATION" >TRANSPORTATION</option>
									<option value="TRAVEL_AND_LOCAL" >TRAVEL_AND_LOCAL</option>
									<option value="WEATHER" >WEATHER</option>
									<option value="APP_WIDGETS" >APP_WIDGETS</option>
									<option value="GAME_ACTION" >GAME_ACTION</option>
									<option value="GAME_ADVENTURE" >GAME_ADVENTURE</option>
									<option value="GAME_ARCADE" >GAME_ARCADE</option>
									<option value="GAME_BOARD" >GAME_BOARD</option>
									<option value="GAME_CARD" >GAME_CARD</option>
									<option value="GAME_CASINO" >GAME_CASINO</option>
									<option value="GAME_CASUAL" >GAME_CASUAL</option>
									<option value="GAME_EDUCATIONAL" >GAME_EDUCATIONAL</option>
									<option value="GAME_MUSIC" >GAME_MUSIC</option>
									<option value="GAME_PUZZLE" >GAME_PUZZLE</option>
									<option value="GAME_RACING" >GAME_RACING</option>
									<option value="GAME_ROLE_PLAYING" >GAME_ROLE_PLAYING</option>
									<option value="GAME_SIMULATION" >GAME_SIMULATION</option>
									<option value="GAME_SPORTS" >GAME_SPORTS</option>
									<option value="GAME_STRATEGY" >GAME_STRATEGY</option>
									<option value="GAME_TRIVIA" >GAME_TRIVIA</option>
									<option value="GAME_WORD" >GAME_WORD</option>
									<option value="FAMILY" >FAMILY</option>
									<option value="FAMILY_ACTION" >FAMILY_ACTION</option>
									<option value="FAMILY_BRAINGAMES" >FAMILY_BRAINGAMES</option>
									<option value="FAMILY_CREATE" >FAMILY_CREATE</option>
									<option value="FAMILY_EDUCATION" >FAMILY_EDUCATION</option>
									<option value="FAMILY_MUSICVIDEO" >FAMILY_MUSICVIDEO</option>
									<option value="FAMILY_PRETEND" >FAMILY_PRETEND</option>
								</select>
								<label>Pilih Kategori</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col m2 s12 offset-m1">
								<button name="type" type="button" onclick="scrape('topselling_free');" value="free" class="btn-large waves-effect waves-light orange">TOP FREE</button>
							</div>
							<div class="input-field col m2 s12">
								<button name="type" type="button" onclick="scrape('topselling_new_free');" value="new" class="btn-large waves-effect waves-light green">TOP NEW</button>
							</div>
							<div class="input-field col m2 s12">
								<button name="type" type="button" onclick="scrape('topgrossing');" value="groosing" class="btn-large waves-effect waves-light blue">TOP GROOSING</button>
							</div>
							<div class="input-field col m2 s12">
								<button name="type" type="button" onclick="scrape('topselling_paid');" value="free" class="btn-large waves-effect waves-light teal">TOP PAID</button>
							</div>
							<div class="input-field col m2 s12">
								<button name="type" type="button" onclick="scrape('topselling_new_paid');" value="new" class="btn-large waves-effect waves-light black">TOP NEW PAID</button>
							</div>
							
							<div class="input-field col m12 s12" style="padding: 10px">
								<button name="type" type="button" onclick="scrape('movers_shakers');" value="groosing" class="btn-large waves-effect waves-light red"><i class="material-icons left">attach_money</i> movers shakers <i class="material-icons right">attach_money</i></button>			
							</div>
						</div>						
					</form>				
				</div>
			</div>
			<br><br>

		</div>
	</div>

	<div class="container">
		<div class="section">

			<!--   Icon Section   -->
			<div class="row">
				<div class="col s12 m12">
					<div class="icon-block">
						<h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
						<h5 class="center">Hasil Scrape</h5>
						<div class="row">
							<div id="hasilScrape"></div>
						</div>						
						<br>
						<div id="loader" class="progress" style="display: none;">
							<div class="indeterminate"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br><br>
	</div>

	<footer class="page-footer orange">
		<div class="container">
			<div class="row">

			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
				Made with ❤ in Gayam
			</div>
		</div>
	</footer>


	<!--  Scripts-->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<script type="text/javascript">

		$(document).ready(function() {
			$('select').material_select();
		});
		(function($){
			$(function(){
				$('.button-collapse').sideNav();
			}); 
		})(jQuery);
		
		var i 			= 0;
		var start 		= 0;
		var num 		= 60;

		function scrape(collections) {
			if (i == 0) $("#hasilScrape").html('');
			var country 	= $('#country').find(":selected").val();
			var lang 		= $('#country').find(":selected").data('hl');
			var category 	= $('#category').find(":selected").val();
			var collections = collections;
			$("#loader").show();
			setTimeout(function () {
				$("#loader").show();
				i++;
				if (i <= 1) {
					$.ajax({
						url: "scrape.php",
						type: "POST",
						data: {country : country,lang : lang,category : category,collections : collections,start : start,num : num},
						success: function(html){
							$("#hasilScrape").append(html);
							$("#loader").hide();
							start = start+60;
							scrape(collections);
						}
					});
				}
			}, 3000)
			
		}


	</script>
	</html>





