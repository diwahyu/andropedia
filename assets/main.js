$(document).on("load", "body", function(){
    $('.tooltipped').tooltip({delay: 50});
    $(".dropdown-trigger").dropdown();
    $('select').material_select();
});

var i       = 0;
var num     = 60;

function scrape(collections, start, i) {

  if (i == 0) $("#hasilScrape").html('');     
  var country   = $('#country').find(":selected").val();
  var lang    = $('#country').find(":selected").data('hl');
  var category  = $('#category').find(":selected").val();     
  var collections = collections;
  var total     = $('#total').find(":selected").val();

  if (country == "" || category == "" || total == "") {
    alert("Pilih Negara, Kategori dan Total");
  }else{
    setTimeout(function () {
      i++;
      if (i <= total) {
        $("#loadingbos").show();
        $(".gas").prop("disabled", true);
        $.ajax({
          url: "scrape.php",
          type: "POST",
          data: {country : country,lang : lang,category : category,collections : collections,start : start,num : num},
          success: function(html){
            $("#hasilScrape").append(html);
            $("#loadingbos").hide();
            start = start+60;
            $(".gas").prop("disabled", false);
            $('.tooltipped').tooltip({delay: 50});
            scrape(collections,start,i);            
          }
        });
      }else{
        Materialize.toast('Data telah di muat ^_^',4000);
      }
    }, 1000)
  }
}

function search() {

  $("#hasilSearch").html('');     
  var country   = $('#country').find(":selected").val();
  var lang      = $('#country').find(":selected").data('hl');
  var pricing   = $('#pricing').find(":selected").val();     
  var rating    = $('#rating').find(":selected").val();
  var keyword    = $('#keyword').val();

  if (pricing == "" || rating == "") {
    alert("Pilih Negara, Pricing dan Rating");
  }else{
    $("#loadingbos").show();
    $(".gas").prop("disabled", true);
    $.ajax({
      url: "scrapesearch.php",
      type: "POST",
      data: {country : country,lang : lang,pricing : pricing,rating : rating,keyword : keyword},
      success: function(html){
        $("#hasilSearch").append(html);
        $("#loadingbos").hide();
        $('.tooltipped').tooltip({delay: 50});
        $(".gas").prop("disabled", false);
      }
    });
  }
}

function bookmark(index,package) {
  var title     = $("#title-"+index).text(); 
  var developer = $("#developer-"+index).text();
  var image     = $("#image-"+index).attr('src');
  var country   = $('#country').find(":selected").val();
  var lang      = $('#country').find(":selected").data('hl');
  var category  = $('#category').find(":selected").val();
  var type      = $("#type").val();
  $.ajax({
    url     : 'bookmark.php',
    data    : {
      id: index,
      package: package,
      title: title,
      developer: developer,
      image: image,
      country:country,
      lang:lang,
      category:category,
      type:type
    },
    type    : "post",
    success: function(result){
      if(result){
        $("#bookmark-"+index).hide();
        $("#unbookmark-"+index).show();
        Materialize.toast('Bookmark success !>_<',4000);
      }
    },
    error: function (jqXHR, textStatus, errorThrown){
      alert('Error get data from ajax');
    }
  });
}
function unbookmark(index,package) {

  $.ajax({
    url     : 'unbookmark.php',
    data    : {
      id: index,
      package: package
    },
    type    : "post",
    success: function(result){
      if(result){
        $("#bookmark-"+index).show();
        $("#unbookmark-"+index).hide();
        Materialize.toast('Bookmark removed !',4000);

      }
    },
    error: function (jqXHR, textStatus, errorThrown){
      alert('Error get data from ajax');
    }
  });
}


function unbookmarklist(index,package) {

  $.ajax({
    url     : 'unbookmark.php',
    data    : {
      id: index,
      package: package
    },
    type    : "post",
    success: function(result){
      if(result){          
        $("#bm-"+index).hide();
        Materialize.toast('Bookmark removed !',4000);

      }
    },
    error: function (jqXHR, textStatus, errorThrown){
      alert('Error get data from ajax');
    }
  });
}

function bookmarkanalytic(index,package,country,lang,type,category) {
  var title     = $("#title-"+index).text(); 
  var developer = $("#developer-"+index).text();
  var image     = $("#image-"+index).attr('src');
  $.ajax({
    url     : 'bookmark.php',
    data    : {
      id: index,
      package: package,
      title: title,
      developer: developer,
      image: image,
      country:country,
      lang:lang,
      category:category,
      type:type
    },
    type    : "post",
    success: function(result){
      if(result){
        $("#bookmark-"+index).hide();
        $("#unbookmark-"+index).show();
        Materialize.toast('Bookmark success !>_<',4000);
      }
    },
    error: function (jqXHR, textStatus, errorThrown){
      alert('Error get data from ajax');
    }
  });
}