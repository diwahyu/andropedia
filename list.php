<?php 
session_start();
require 'conf.php';

if (!isset($_SESSION['access_token']) && !isset($_SESSION['email_login']) || empty(setcookie('user', '')) ) {
	header("Location:login.php");
}
$email    	= $_SESSION['email_login'];

$query 		= mysql_query("select * from tb_bookmark where email='$email' ", $connection);

$root = "http://localhost/andropedia/";

$rows = mysql_num_rows($query);

$date_now = date("Y-m-d");
$expired = false;
$aktif	= false;
if (isset($_SESSION['license_type'])) {
	if ($_SESSION['license_type'] == "Pro") {
		if ($date_now <= $_SESSION['end_license']) {
			$aktif = true;			
		}else{						
			$expired = true;
		}
	}
}

require 'header_templates.php';

?>

    
      		<!-- //////////////////////////////////////////////////////////////////////////// -->
      		<!-- START CONTENT -->
      		<section id="content">
      			<!--breadcrumbs start-->
      			<!-- <div id="breadcrumbs-wrapper"> -->
      				<!-- Search for small screen -->
      				<!-- <div class="header-search-wrapper grey lighten-2 hide-on-large-only"> -->
      					<!-- <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize"> -->
      				<!-- </div> -->
      			<!-- </div> -->
      			<!--breadcrumbs end-->
      			<!--start container-->
      			<div class="container">
      				<?php if ($expired) {
      					echo '<div class="card-panel red white-text center">Paket lisensi anda telah expired, silahkan perbaru lagi. Paket Anda kembali ke Standart</div>';
      				}?>		
      				<div class="section">

      					<!--   Icon Section   -->
      					<div class="row">
      						<div class="col s12 m12">
      							<div class="icon-block">
      								<h2 class="center light-blue-text"><i class="material-icons">book</i></h2>
      								<h5 class="center">Bookmark List</h5>
      								<div class="row">
      									<?php
      									if ($rows > 0) {
      										$i=0;

      										while($row = mysql_fetch_assoc($query)) {      											
      											echo '
      											<div class="col s12" id="bm-'.$i.'">
      											<div class="card gradient-45deg-deep-purple-blue">
      											<div class="card-content white-text" style="overflow: hidden;">              
      											<div class="col s12 m2">
      											<img src="'.$row["image"].'" alt="" class="responsive-img"> 
      											</div>
      											<div class="col s12 m5">
      											<span class="card-title">'.$row["title"].' </span>
      											<b>'.$row["developer"].' </b> 
      											<p class="white-text">'.$row["packagename"].'</p>
      											<button class="waves-effect waves-blue btn-flat btn-small white-text tooltipped gradient-45deg-blue-indigo" data-position="bottom" data-tooltip="Remove Bookmark" onclick="javascript:unbookmarklist('.$i.',\''.$row["packagename"].'\');" id="unbookmark-'.$i.'">
      											<i class="material-icons">bookmark</i>
      											</button>
      											</div>
      											<div class="col s12 m5">
      											<div class="card light-blue lighten-5">
      											<div class="card-content">';
      											if ($aktif) {
      												echo '<a target="_blank" class="waves-effect waves-light btn box-shadow-none gradient-45deg-amber-amber" href="'.$root.'analytics.php?req='.$row["packagename"].'&c='.$row['country'].'&lang='.$row['lang'].'&type='.$row['type'].'&category='.$row['category'].'">Analyze</a>';
      												echo '<a target="_blank" class="waves-effect waves-light btn box-shadow-none gradient-45deg-purple-deep-orange" href="https://apkpure.com/'.urlencode($row["title"]).'/'.$row["packagename"].'/download?from=details">Download Via Apkpure</a>';
      											}else{
      												echo '<a target="_blank" class="waves-effect waves-light btn box-shadow-none blue" disabled href="#">Analyze</a>';
      												echo '<a target="_blank" class="waves-effect waves-light btn box-shadow-none blue" disabled href="#">Download Via Apkpure</a>';
      											}

      											echo ' <hr>
      											<a target="_blank" class="waves-effect waves-light btn box-shadow-none light-green" href="https://play.google.com/store/apps/details?id='.$row["packagename"].'&hl='.$row['lang'].'&gl='.$row['country'].'">Playstore</a>
      											<a target="_blank" class="waves-effect waves-light btn box-shadow-none green" href="https://play.google.com/store/apps/details?id='.$row["packagename"].'&hl=id&gl='.$row['country'].'">Playstore (BAHASA)</a>
      											<a target="_blank" class="waves-effect waves-light btn box-shadow-none teal" href="https://play.google.com/store/apps/developer?id='.str_replace(' ', '+', $row["developer"]).'">Developer Page</a>
      											<hr>
      											<a target="_blank" class="waves-effect waves-light btn box-shadow-none cyan" href="http://www.appbrain.com/app/'.$row["packagename"].'">App Brain</a>';
      											if ($aktif) {
      												echo'
      												<a target="_blank" class="waves-effect waves-light btn box-shadow-none blue" href="http://www.appbrain.com/dev/'.urlencode($row["developer"]).'">App Brain Developer Page</a>
      												<hr>
      												<a target="_blank" class="waves-effect waves-light btn box-shadow-none black" href="https://searchman.com/android/app/us/'.$row["packagename"].'/en/">Searchman</a>
      												<a target="_blank" class="waves-effect waves-light btn box-shadow-none blue-grey" href="https://searchman.com/android/publisher/us/'.urlencode($row["developer"]).'/en/">Searchman Developer Page</a>';
      											}else{
      												echo'
      												<a target="_blank" class="waves-effect waves-light btn box-shadow-none blue" disabled href="#">App Brain Developer Page</a>
      												<hr>
      												<a target="_blank" class="waves-effect waves-light btn box-shadow-none black" disabled href="#">Searchman</a>
      												<a target="_blank" class="waves-effect waves-light btn box-shadow-none blue-grey" disabled href="#">Searchman Developer Page</a>
      												';
      											}
      											echo'


                            </div>
      											</div>
      											</div>
      											</div>
      											</div>
      											</div>
      											';
      											$i++;}
      										}
      										?>
      									</div>						

      								</div>
      							</div>
      						</div>
      					</div>
      					<br><br>
      				</div>

      				<!--end container-->
      			</section>
      			<!-- END CONTENT -->
      			<!-- //////////////////////////////////////////////////////////////////////////// -->      		
      		</div>
      		<!-- END WRAPPER -->
      	</div>
      	<!-- END MAIN -->
      	<!-- //////////////////////////////////////////////////////////////////////////// -->
<?php 
require 'footer_templates.php';
 ?>