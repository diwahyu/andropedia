<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Model {

function __construct() {
	parent::__construct();
}
function standart(){
	$query = $this->db->query("SELECT * FROM mdl_license WHERE license_type = 'Standart' ");
	$total = $query->num_rows();
	return $total;
}
function pro(){
	$query = $this->db->query("SELECT * FROM mdl_license WHERE license_type = 'Pro' ");
	$total = $query->num_rows();
	return $total;
}
function totalli(){
	$query = $this->db->query("SELECT * FROM mdl_license");
	$total = $query->num_rows();
	return $total;
}


}

/* End of file home.php */
/* Location: ./application/modules/dashboard/models/home.php */