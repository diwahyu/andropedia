<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mdl_license extends CI_Model {

function __construct() {
	parent::__construct();
}

public function ambil_data($num, $offset){
    $this->db->order_by('id', 'ASC');
    $data = $this->db->get('mdl_license', $num, $offset);
    return $data->result_array();
}

function get_license(){
	$query = $this->db->get('mdl_license');
	return $query->result_array();
}
function search($email){
    $this->db->like('email',$email);
    $query = $this->db->get('mdl_license');
    // $query = "SELECT * FROM mdl_license WHERE email LIKE '%$email%' ";
    return $query->result_array();
}
function set_license(){
    $this->load->helper('url');
    $data = array(
        'fullname' => $this->input->post('fullname'),
        'email' => $this->input->post('email'),
        'password' => md5($this->input->post('password')),
        'license_type' => $this->input->post('license_type'),
        'start_license' => date('y-m-d'),
        'create_date' => date('y-m-d')
    );
    return $this->db->insert('mdl_license', $data);
}

function get_license_id($id){
    $query = $this->db->get_where('mdl_license', array('id' => $id));
    return $query->row_array();
}
function edit_license($id){
    $this->load->helper('url');
    $data = array(
        'license_type' => $this->input->post('license_type'),
        'start_license' => $this->input->post('start_license'),
        'end_license' => $this->input->post('end_license'),
        'edit_date' => date('y-m-d')
    );
    $this->db->where('id',$id);
    return $this->db->update('mdl_license',$data);
}

function delete($id){
	$this->db->where('id', $id);
	$this->db->delete('mdl_license');
}


}