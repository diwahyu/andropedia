<?php
class License extends MX_Controller 
{

    function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model('mdl_license');
        Modules::run('secure_tings/ni_admin');
    }
    public function index($id=NULL){
        $this->load->library('pagination');
        $jml = $this->db->get('mdl_license');
        //pengaturan pagination
        $config['base_url'] = base_url().'license/index';
        $config['total_rows'] = $jml->num_rows();
        $config['per_page'] = '5';
        $config['first_page'] = 'Awal';
        $config['last_page'] = 'Akhir';
        $config['next_page'] = '&laquo;';
        $config['prev_page'] = '&raquo;';
        //inisialisasi config
        $this->pagination->initialize($config);
        //buat pagination
        $data['halaman'] = $this->pagination->create_links();
        //tamplikan data
        $data['records'] = $this->mdl_license->ambil_data($config['per_page'], $id);
        $data['section'] = "Admin";
        $data['subtitle'] = "License";
        $data['page_title'] = "List of license";
        $data['module']="license";
        $data['view_file']="license";
        echo Modules::run('template/admin', $data);
    }
    public function search(){
        $email = $this->input->get('email');
        $data['records'] = $this->mdl_license->search($email);
        $data['section'] = "Admin";
        $data['subtitle'] = "License";
        $data['page_title'] = "List of license";
        $data['module']="license";
        $data['view_file']="search";
        echo Modules::run('template/admin', $data);
    }

    public function create(){
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('fullname', 'Fullname', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        
        if ($this->form_validation->run() === FALSE){
            $data['title'] = "Create License";
            $data['section'] = "Admin";
            $data['subtitle'] = "License";
            $data['page_title'] = "Create license";
            $data['module']="license";
            $data['view_file']="create";
            echo Modules::run('template/admin', $data);
        }else{
            $email = $this->input->post('email');
            $this->db->where('email', $email);
            $query = $this->db->get('mdl_license');
            $count_row = $query->num_rows();
            if($count_row > 0){
                $this->session->set_flashdata('msg', '<div id="alert-message" class="alert alert-success text-center">Email sudah ada!</div>');
                redirect('license/create');
            }else{
                $this->mdl_license->set_license($id);
                $this->session->set_flashdata('msg', '<div id="alert-message" class="alert alert-success text-center">Data added successfully!</div>');
                redirect('license');
            }
        }
    }

    public function submit(){
        $this->mdl_license->ceking();
        redirect('license');
    }

    public function edit($id){
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('license_type', 'License Type', 'required');
        $this->form_validation->set_rules('start_license', 'Start License', 'required');
        // $this->form_validation->set_rules('end_license', 'End License', 'required');

        $data['title'] = "Edit License";

        if ($this->form_validation->run() === FALSE)
        {
    	    $data['section'] = "Admin";
    	    $data['subtitle'] = "License";
    	    $data['page_title'] = "List of license";
    	    $data['module']="license";
    	    $data['view_file']="edit";
    		$data['records'] = $this->mdl_license->get_license_id($id);
    	    echo Modules::run('template/admin', $data); 
        }
        else
        {
            $this->mdl_license->edit_license($id);
            $this->session->set_flashdata('msg', '<div id="alert-message" class="alert alert-success text-center">Data updated successfully!</div>');
            redirect('license');
        }
                 
    }

	public function delete($id){
		$this->mdl_license->delete($id);
		$this->session->set_flashdata('msg', '<div id="alert-message" class="alert alert-success text-center">Data deleted successfully!</div>');
		redirect('license');
	}

}