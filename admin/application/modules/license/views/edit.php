<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
  <div class="widget-body">

    <h4><?php echo $title; ?></h4>
    <?php echo validation_errors(); ?>
  
  <?php echo form_open('license/edit/'.$records['id']); ?>
    <label for="">Fullname</label>
    <input type="text" value="<?= $records['fullname']; ?>" readonly>
    <label for="">Email</label>
    <input type="text" value="<?= $records['email']; ?>" readonly>
    <label for="">License Type</label>
    <select name="license_type" id="">
      <option value="Standart">Standart</option>
      <option value="Pro" <?php if($records['license_type'] == 'Pro'){ echo 'selected';}else{}?>>Pro</option>
    </select>
    <label>Start License</label>
    <input type="date" name="start_license" placeholder="Fullname" value="<?= $records['start_license']; ?>">
    <label>End License</label>
    <input type="date" name="end_license" placeholder="Email" value="<?= $records['end_license']; ?>">
    <label></label>
    <input type="submit" name="submit" value="Edit" class="btn btn-warning">
  
  </form>

  </div>