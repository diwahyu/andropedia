<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
 <div class="widget-body">
      <a href="<?php echo site_url('license/create');?>" class="btn btn-primary pull-left">Create License</a>
        <form action="<?php echo site_url('license/search');?>" method ="get" id="custom-search-form" class="form-search form-horizontal pull-right">
            <div class="input-append span12">
                <input type="text" name="email" class="search-query" placeholder="Search">
                <button type="submit" class="btn btn-primary"><i class="icon-search"></i></button>
            </div>
        </form>
  </br>
  </br>
  <?php echo $this->session->flashdata('msg'); ?>
     
        <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                    <td width="25" align="center">No</td>
                    <td>Fullname</td>
                    <td>Email</td>
                    <td>License Type</td>
                    <td>Start License</td>
                    <td>End License</td>
                    <td align="center" width="150"><b>Option</b></td>
                </tr>
            </thead>
            <tbody>
                <?php
            if($records != NULL){
                // $no = 0;
                $no = $this->uri->segment('3') + 0;
                foreach ($records as $row){
                    $no++;
                    $edit_url = base_url().'license/edit/'.$row['id'];
                    $delete_url = base_url().'license/delete/'.$row['id'];
                  ?>
                <tr>
                    <td align="center"><?= $no; ?></td>
                    <td><?= $row['fullname']; ?></td>
                    <td><?= $row['email']; ?></td>
                    <td><?= $row['license_type']; ?></td>
                    <td><?= $row['start_license']; ?></td>
                    <td><?= $row['end_license']; ?></td>
                    <td align="center" colspan="2">
                        <a href="<?= $edit_url ?>" class="btn btn-warning">Edit</a>
                        <a href="<?= $delete_url ?>" class="btn btn-danger">Delete</a>
                    </td>
                   
                </tr>
                <?php }
            }else{
                 ?>
                    <tr>
                        <td colspan="8" align="center">
                            <center>Kosong</center>
                        </td>
                    </tr>
             <?php } ?>
            </tbody>
        </table>
        </br>
    <?php 
    // echo $this->pagination->create_links();
    ?>
    <div class="halaman">Halaman : <?php echo $halaman;?></div>
    </div>

  <script type="text/javascript">
window.setTimeout(function() {
    $("#alert-message").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 5000);

</script>
