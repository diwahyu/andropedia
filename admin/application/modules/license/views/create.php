<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

  <div class="widget-body">

    <h4><?php echo $title; ?></h4>
    <?php echo validation_errors(); ?>
    <?php echo form_open('license/create'); ?>
    <?php echo $this->session->flashdata('msg'); ?>
    <label>Fullname</label>
    <input type="text" name="fullname" placeholder="Fullname">
    <label>Email</label>
    <input type="email" name="email" placeholder="Email" id="email">
    <!-- <p id="pesan"></p> -->
    <label for="">Password</label>
    <input type="hidden" name="license_type" value="Standart">
    <input type="password" name="password" placeholder="Password">
    <label></label>
    <input type="submit" name="submit" value="Create" class="btn btn-primary">
  
  </form>
  </div>