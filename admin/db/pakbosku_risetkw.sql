-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2018 at 05:30 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pakbosku_risetkw`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('3a35419e96d63498ffc897fb92c740e2', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 1533516021, ''),
('87817ef64eb24dd15dedef32b5cfe6ac', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 1533869217, ''),
('e714c5c158dbf06aea027e03553d1546', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 1533365599, 'a:7:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"1\";s:10:\"user_fname\";s:5:\"Julie\";s:10:\"user_lname\";s:7:\"Eustine\";s:10:\"user_email\";s:13:\"oti@gmail.com\";s:10:\"user_group\";s:1:\"1\";s:9:\"logged_in\";b:1;}'),
('fece59397d633f74ed0c5b9ff7740233', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 1533871349, '');

-- --------------------------------------------------------

--
-- Table structure for table `license`
--

CREATE TABLE `license` (
  `id` int(11) NOT NULL,
  `fbid` varchar(255) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `license_type` varchar(255) NOT NULL,
  `start_license` date NOT NULL,
  `end_license` date NOT NULL,
  `create_date` date NOT NULL,
  `edit_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `license`
--

INSERT INTO `license` (`id`, `fbid`, `fullname`, `email`, `license_type`, `start_license`, `end_license`, `create_date`, `edit_date`) VALUES
(8, '', 'hikam', 'hikam@gmail.com', '', '2018-06-28', '0000-00-00', '2018-06-28', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `mdl_license`
--

CREATE TABLE `mdl_license` (
  `id` int(11) NOT NULL,
  `fbid` varchar(255) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `license_type` varchar(25) NOT NULL,
  `start_license` date NOT NULL,
  `end_license` date NOT NULL,
  `create_date` date NOT NULL,
  `edit_date` date NOT NULL,
  `waktu_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(2) NOT NULL,
  `browser` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mdl_license`
--

INSERT INTO `mdl_license` (`id`, `fbid`, `fullname`, `email`, `password`, `license_type`, `start_license`, `end_license`, `create_date`, `edit_date`, `waktu_login`, `status`, `browser`) VALUES
(21, '', 'popeye', 'popeye@pop.com', '8b848a152ad86d9fec34c7c291c66858', 'Standart', '2018-06-30', '2018-06-29', '2018-06-30', '2018-08-04', '2018-08-10 03:01:11', 0, ''),
(22, '', 'Dinar Wahyu Wibowo', 'dinarwahyu13@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Pro', '2018-06-01', '2018-10-13', '2018-06-30', '2018-07-19', '2018-08-10 03:28:11', 1, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36');

-- --------------------------------------------------------

--
-- Table structure for table `m_group`
--

CREATE TABLE `m_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_group`
--

INSERT INTO `m_group` (`id`, `name`, `role`) VALUES
(1, 'Admin123', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `m_proxy`
--

CREATE TABLE `m_proxy` (
  `id_proxy` int(11) NOT NULL,
  `proxy` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_uploads`
--

CREATE TABLE `m_uploads` (
  `id` int(11) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `raw_name` varchar(200) NOT NULL,
  `file_type` varchar(20) NOT NULL,
  `full_path` varchar(1000) NOT NULL,
  `upload_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_uploads`
--

INSERT INTO `m_uploads` (`id`, `file_name`, `raw_name`, `file_type`, `full_path`, `upload_date`) VALUES
(1, 'tttt', 'awesome-code-typography-hd-wallpaper-1920x1080-2616.jpg', 'image/jpeg', 'C:/xampp/htdocs/Twiga/docs/awesome-code-typography-hd-wallpaper-1920x1080-2616.jpg', '2015-09-01'),
(2, 'ddsd', 'images.jpg', 'image/jpeg', 'C:/xampp/htdocs/Twiga/docs/images.jpg', '2015-09-01'),
(4, 'sxsd', 'images1.jpg', 'image/jpeg', 'C:/xampp/htdocs/Twiga/docs/images1.jpg', '2015-09-01');

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `user_group` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_users`
--

INSERT INTO `m_users` (`id`, `f_name`, `l_name`, `username`, `phone`, `email`, `password`, `title`, `user_group`) VALUES
(1, 'Julie', 'Eustine', 'julie', '0703568592', 'oti@gmail.com', '0a692f089b30b507bc881486d21a15f4ce7534ba02cf4d9bcc0062375fdcde1a364a9370593c274e0f0632fc7ae7448bdee5d267b64685f07bd7192128f6ff38', 'Admin', 1),
(3, 'Dinar', 'Wibowo', 'admin', '85730277132', 'dinarwahyu13@gmail.com', '73a91cf58a1f5a9a62d9613e7778ca77062d350db58498960082e2fa02399881b389dafabf97ef11218d91aafb8deb3c0b9a0ce61b00c0d60a8d1f926891cbbd', 'mr', 1),
(4, 'Dinar', 'Wibowo', 'admin', '85730277132', 'dinarwahyu13@gmail.com', '73a91cf58a1f5a9a62d9613e7778ca77062d350db58498960082e2fa02399881b389dafabf97ef11218d91aafb8deb3c0b9a0ce61b00c0d60a8d1f926891cbbd', 'mr', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_bookmark`
--

CREATE TABLE `tb_bookmark` (
  `id` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `packagename` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `developer` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `lang` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bookmark`
--

INSERT INTO `tb_bookmark` (`id`, `email`, `packagename`, `title`, `developer`, `image`, `country`, `type`, `category`, `lang`, `create_date`) VALUES
(12, 'dinarwahyu13@gmail.com', 'com.ronimusic.asd', 'Amazing Slow Downer', 'Roni Music', '//lh4.ggpht.com/0Mpncmv8G_joSxrCIxmdMNjdgnjUgTkb-foKSJtshMle1qEZXP1_F7zGpN2pYJ8ja7s=w170', '', '', '', '', '2018-08-03 03:59:50'),
(15, 'dinarwahyu13@gmail.com', 'wp.wattpad', 'Wattpad  ðŸ“–  Free Books', 'Wattpad.com', '//lh3.googleusercontent.com/OhhxSvYx7d5-_cb0lwFYPLmzL7H04i4ZFGEnLnZcyDrcvktpA3iDe3av-K1HIw9d_V4=w170', 'AL', 'topselling_free', 'BOOKS_AND_REFERENCE', 'en', '2018-08-04 03:14:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `license`
--
ALTER TABLE `license`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_license`
--
ALTER TABLE `mdl_license`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_group`
--
ALTER TABLE `m_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_proxy`
--
ALTER TABLE `m_proxy`
  ADD PRIMARY KEY (`id_proxy`);

--
-- Indexes for table `m_uploads`
--
ALTER TABLE `m_uploads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_bookmark`
--
ALTER TABLE `tb_bookmark`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `license`
--
ALTER TABLE `license`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mdl_license`
--
ALTER TABLE `mdl_license`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `m_group`
--
ALTER TABLE `m_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_proxy`
--
ALTER TABLE `m_proxy`
  MODIFY `id_proxy` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `m_uploads`
--
ALTER TABLE `m_uploads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_bookmark`
--
ALTER TABLE `tb_bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
